﻿using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using wwlp.Application.Interfaces;
using wwlp.Domain.Entities;
using wwlp.MVC.ViewModels;

namespace wwlp.MVC.ViewModels.UI
{
    public class UIBase
    {
        public readonly IUserAppObject _userAppObject;
        public UserViewModel UserCurrent { get; set; }

        public UIBase(IUserAppObject userAppObject)
        {
            /*
             * Testando mapper obtendo 1 usuadio da camada infra
             *
             * Neste momento deve ocorre a verificaça de cookies antes de bater no banco
             * Trabalhar com SSO aqui
             */
            _userAppObject = userAppObject;
            var usersVM = Mapper.Map<IEnumerable<User>, IEnumerable<UserViewModel>>(_userAppObject.GetByFilter());
            UserCurrent = usersVM.FirstOrDefault();
        }
    }
}

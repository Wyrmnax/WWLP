﻿using Microsoft.Extensions.DependencyInjection;
using wwlp.Application;
using wwlp.Application.Interfaces;

namespace wwlp.Cross.DependencyResolver
{
    public class ApplicationModule
    {
        public ApplicationModule(IServiceCollection services)
        {
            services.AddSingleton(typeof(IAppObjectsBase<>), typeof(AppObjectsBase<>));
            services.AddSingleton<IUserAppObject, UserAppObject>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace wwlp.MVC.ViewModels
{
    public class UserViewModel
    {
        [Key]
        public int ID { get; set; }
        public bool Active { get; set; }
        public DateTime? DataAdded { get; set; }
        public DateTime? DataChanged { get; set; }
    }
}

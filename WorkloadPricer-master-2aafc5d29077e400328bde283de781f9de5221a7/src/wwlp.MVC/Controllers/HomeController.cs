﻿using Microsoft.AspNetCore.Mvc;
using wwlp.Application.Interfaces;
using wwlp.MVC.ViewModels;
using wwlp.MVC.ViewModels.UI;

namespace wwlp.MVC.Controllers
{
    public class HomeController : ControllerBase
    {
        private HomeUI UI { get; set; }
        private readonly IUserAppObject _userAppObject;

        public HomeController(IUserAppObject userAppObject)
        {
            _userAppObject = userAppObject;
            UI = new HomeUI(userAppObject);
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Title"] = "Dashboard";
            return View(UI);
        }

        [HttpGet]
        public IActionResult About()
        {
            ViewData["Title"] = "About";
            return View(UI);
        }
    }
}

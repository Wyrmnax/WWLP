﻿using Microsoft.Extensions.DependencyInjection;
using wwlp.Domain.BusinessObjects;
using wwlp.Domain.Interfaces.BusinessObject;

namespace wwlp.Cross.DependencyResolver
{
    public class DomainModule
    {
        public DomainModule(IServiceCollection services)
        {
            services.AddSingleton(typeof(IBusinessObjectBase<>), typeof(BusinessObjectBase<>));
            services.AddSingleton<IUserBO, UserBO>();
        }
    }
}

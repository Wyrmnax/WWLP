﻿using Microsoft.Extensions.DependencyInjection;
using wwlp.Domain.Interfaces.Repository;
using wwlp.Infra.Data.Repositories;

namespace wwlp.Cross.DependencyResolver
{
    public class InfraModule
    {
        public InfraModule(IServiceCollection services)
        {
            services.AddSingleton(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));
            services.AddSingleton<IUserRepository, UserRepository>();
        }
    }
}

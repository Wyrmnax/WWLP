using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace wwlp.MVC.Controllers
{
    public class CustomerConfigController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Title"] = "Configurations list";
            return View();
        }

        [HttpGet]
        public IActionResult New()
        {
            ViewData["Title"] = "New Customer Configuration";
            return View();
        }
    }
}
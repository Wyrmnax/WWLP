﻿using AutoMapper;

namespace wwlp.MVC.Automapper
{
    public class AutoMapperConfig
    {
        public AutoMapperConfig()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });
        }
    }
}

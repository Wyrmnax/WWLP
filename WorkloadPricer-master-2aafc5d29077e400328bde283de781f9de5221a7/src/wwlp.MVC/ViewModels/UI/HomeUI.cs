﻿using wwlp.Application.Interfaces;

namespace wwlp.MVC.ViewModels.UI
{
    public class HomeUI : UIBase
    {
        public HomeUI(IUserAppObject userAppObject)
            : base(userAppObject)
        {

        }
    }
}

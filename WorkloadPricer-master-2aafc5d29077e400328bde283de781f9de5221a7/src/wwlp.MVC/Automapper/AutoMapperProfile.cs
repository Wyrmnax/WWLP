﻿using AutoMapper;
using wwlp.Domain.Entities;
using wwlp.MVC.ViewModels;

namespace wwlp.MVC.Automapper
{
    public class AutoMapperProfile : Profile
    {
        public override string ProfileName
        {
            get { return "WorkloadPricerProfile"; }
        }

        public AutoMapperProfile()
        {
            CreateMap<User, UserViewModel>().ReverseMap();
        }
    }
}
